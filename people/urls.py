from django.conf.urls import url

from . import views

app_name = 'people'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^list/$', views.ListView.as_view(), name='list'),
    url(r'^add/$', views.AddView.as_view(), name='add'),
]