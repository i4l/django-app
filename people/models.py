from __future__ import unicode_literals

from django.db import models

class People(models.Model):
    name = models.CharField(max_length=200, blank=False)
    email = models.EmailField(max_length=200, blank=False, unique=True)

    def __str__(self):
        return self.name
