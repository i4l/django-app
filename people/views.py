from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views import View
from django.views import generic
from django.views.generic.edit import CreateView

from .models import People

class IndexView(View):
    template_name = 'people/index.html'

    def get(self, request):
        return render(request, self.template_name)

class ListView(generic.ListView):
    template_name = 'people/list.html'
    context_object_name = 'people_list'
    
    def get_queryset(self):
        return People.objects.order_by('name')

class AddView(CreateView):
    model = People
    fields = ['name', 'email']
    template_name = 'people/add.html'
    success_url = reverse_lazy('people:add')
