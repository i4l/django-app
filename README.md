A Django challenge for a Software Engineer position.

Make an application which stores names and email addresses in a database (SQLite is fine). 
  a) Has welcome page in http://localhost/ 
      - this page has links to list and create functions
  b) Lists all stored names / email address in http://localhost/list
  c) Adds a name / email address to the database in http://localhost/add
      - should validate input and show errors